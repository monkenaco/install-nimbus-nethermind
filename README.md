Скрипт `install-nimbus-nethermind.sh` выполняет стандартную установку и запуск consensus client **Nimbus** и execution client **Nethermind** как службы Systemd `nimbus_beacon_node.service` и `nethermind.service`. Оба необходимы для работы ноды Ethereum.

**Установка:**

1. Загрузите файлы `install-nimbus-nethermind.sh`, `nimbus_beacon_node.service` и `nethermind.service` на сервер.
2. Отредактируйте файл `nimbus_beacon_node.service` добавив в конце ExecStart= опцию --suggested-fee-recipient=<адрес для получения fee>
3. Сделайте файл `install-nimbus-nethermind.sh` исполняемым.

```sh
chmod +x install-nimbus-nethermind.sh
```

4. Запустите скрипт установки.

```sh
./install-nimbus-nethermind.sh
```

**Управление службами**

**Проверка статуса:**

```sh
systemctl status nimbus_beacon_node
```

```sh
systemctl status nethermind
```

**Мониторинг:**

```sh
journalctl -u nimbus_beacon_node -f
```

```sh
journalctl -u nethermind -f
```

**Остановка:**

```sh
sudo systemctl stop nimbus_beacon_node
```

```sh
sudo systemctl stop nethermind
```

**Запуск:**

> Перед запуском Nethermind должен быть запущен Nimbus

```sh
sudo systemctl start nimbus_beacon_node
```

```sh
sudo systemctl start nethermind
```

**Импорт ключей Puffer**

```sh
cd ~/puffer/coral
```

```sh
cargo run --bin coral-cli validator keygen .... --password-file ~/puffer/password.txt --output-file ~/puffer/registration.json
```

```sh
cp -v ~/puffer/coral/etc/keys/bls_keys/<replace> ~/nimbus-beacon/validator_keys/<replace>.json
```

```sh
nimbus_beacon_node deposits import --data-dir=/root/nimbus-beacon /root/nimbus-beacon/validator_keys
```

```sh
sudo systemctl restart nimbus_beacon_node
```

**Полезные ссылки:**
https://docs.puffer.fi/nodes/setup
https://nimbus.guide/quick-start.html
https://docs.nethermind.io/get-started/installing-nethermind
