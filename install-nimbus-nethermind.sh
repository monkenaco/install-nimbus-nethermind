sudo apt-get update -y
sudo apt-get upgrade -y

sudo apt-get install software-properties-common build-essential libssl-dev pkg-config ufw git screen tree chrony -y

# Add the nimbus repository
echo 'deb https://apt.status.im/nimbus all main' | sudo tee /etc/apt/sources.list.d/nimbus.list
# Import the GPG key
sudo curl https://apt.status.im/pubkey.asc -o /etc/apt/trusted.gpg.d/apt-status-im.asc

# Add the nethermindeth repository
sudo add-apt-repository ppa:nethermindeth/nethermind -y

# Update repository files
sudo apt-get update -y

# Setting UFW
echo =======================================================
echo Setting UFW
echo =======================================================
sudo ufw default deny incoming
sudo ufw default allow outgoing
sudo ufw allow ssh
sudo ufw allow 9000
sudo ufw allow 30303
sudo ufw enable

# Install Nimbus components
echo =======================================================
echo Installing consensus client
echo =======================================================
sudo apt-get install -y nimbus-beacon-node nimbus-validator-client
sudo mkdir /root/nimbus-beacon
sudo mkdir /root/nimbus-beacon/validator_keys
sudo mkdir /root/nimbus-beacon/secrets
# Move the unit file to the systemd directory
sudo cp nimbus_beacon_node.service /etc/systemd/system
# Optionally, enable the service to start on boot
#sudo systemctl enable mindnimbus-beacon-node

# Install Nethermind
echo =======================================================
echo Installing execution client
echo =======================================================
sudo apt-get install nethermind -y
sudo mkdir /root/nethermind
openssl rand -hex 32 | sudo tee /root/nethermind/jwt-secret.hex
# Move the unit file to the systemd directory
sudo cp nethermind.service /etc/systemd/system
# Optionally, enable the service to start on boot
#sudo systemctl enable nethermind

# Installing Rust
echo =======================================================
echo Installing Rust
echo =======================================================
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

# Installing Puffer
echo =======================================================
echo Installing Puffer
echo =======================================================
mkdir /root/puffer
openssl rand -hex 32 | sudo tee /root/puffer/password.txt
cd /root/puffer
git clone https://github.com/PufferFinance/coral
cd /root/puffer/coral
cargo build --release

# Sync from a trusted node
echo =======================================================
echo Sync from a trusted node https://sync-mainnet.beaconcha.in
echo =======================================================
/usr/bin/nimbus_beacon_node --network:mainnet --web3-url=http://127.0.0.1:8551 --jwt-secret=/root/nethermind/jwt-secret.hex --data-dir=/root/nimbus-beacon trustedNodeSync --trusted-node-url=https://sync-mainnet.beaconcha.in

# Start the service
echo =======================================================
echo Start the service
echo =======================================================
# Reload the systemd daemon
sudo systemctl daemon-reload
# Start the service
sudo systemctl start nimbus_beacon_node
sudo systemctl start nethermind